#!/bin/bash 

path=$(readlink -f $1) #chemin du fichier à sauvegarder
#readlink -f permet d'avoire le chemin du répertoire qu'on veut sauvegarder

echo $path > $path/road.txt

#J'enregistre le chemin du repertoire dans un fichier texte road.txt

archive=/tmp/backup/$(date +%Y_%m_%d_%H_%M)_$(basename $1).tar.gz

#archive représente le chemin du repertoire ou il va etre enregistré en tar.gz
#Dans notre cas, il sera enregistré dans /tmp/backup 
#la commande date avec les arguments %Y/%m/%d/%H/%M va me donner l'année/mois/jour/heure/minutes
#la commande basename va juste ajouter le nom du repertoire enregistré 

tar cf $archive $1
#la commande tar va compresser le dossier / fichier
#l'argument c crée l'archive / f demande de compresser le dossier / fichier donné en paramètre  
