#!/bin/bash
echo "[...]Checking internet connection[...]"
if ping -c 4 google.com;  then echo "[...]Internet access OK[...]" #J'effectue la commande ping 4 fois sur le dns de google et j'afficher un texte si cela fonctionne  
else  #Si cela ne fonctionne pas, je demande à l'utilisateur de verifier sa configuration  
	echo "[/!\]Not connected to Internet[/!\]" 
	echo "[/!\]Please check configuration[/!\]"
fi 
